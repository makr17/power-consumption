# Power Consumption

Queries database of stored power meter readings and produces timeseries graphs.

Expects `DATABASE_URL` to be set in the environment to a valid db connect url.

I'm using PostgreSQL, but there's nothing magical about structure.
`power` table looks like

```
                         Table "public.power"
  Column  |           Type           | Collation | Nullable | Default 
----------+--------------------------+-----------+----------+---------
 sampled  | timestamp with time zone |           | not null | 
 meter_id | integer                  |           | not null | 
 value    | integer                  |           | not null | 
Indexes:
    "power_pkey" PRIMARY KEY, btree (sampled, meter_id)
    "power_u_value" UNIQUE, btree (meter_id, value)
```

`meter_id` is in there but mostly vestigial, I'm only storing values from my meter.

One could easily do this in a db other than PostgreSQL, but it would require a bit of rework, mainly because I'm using psycopg2 for the connection.

App currently just queries last 7 days and produces three charts
* a simple bar chart of daily consumption
* a bar chart of detail incremental consumption
* a line chart of increasing meter values over time

## Where does the data come from

I use
[load-pwp-tail](https://gitlab.com/makr17/load-pwp-tail)
to take the radio-broadcast data from my smart power meter
and store the resulting sample data in the above database.
