import os

import altair as alt
import streamlit as st 

import pandas as pd 
import numpy as np

import psycopg2

# Initialize connection.
# Uses st.cache_resource to only run once.
@st.cache_resource
def init_connection():
    return psycopg2.connect(os.environ.get("DATABASE_URL"))

conn = init_connection()

# build dataframe from query and handle
# pd.read_sql() _works_, but is chatty about how this isn't sqlalchemy
# leading _ on _conn tells streamlit not to try hashing it
@st.cache_data(ttl=600)
def run_query(query, _conn):
    with _conn.cursor() as cur:
        cur.execute(query)
        data = cur.fetchall()
        cols = [elt[0] for elt in cur.description]
        df = pd.DataFrame(data=data, columns=cols)
        return df

# TODO: take input for date/time range to query
# build dataframe
query = """
select
  sampled
  , value
from power
where
  sampled >= current_date - 7
order by sampled;
"""
df = run_query(query, conn)

# calculate incremental values
df["consumed"] = df["value"] - df["value"].shift()
# calculate date across the df
df["sampled_day"] = df["sampled"].dt.date
# and build a second df with values grouped/summed by day
dfday = df.groupby(["sampled_day"]).sum()

# Define the base time-series chart.
def get_chart(data, field, units):
    hover = alt.selection_single(
        fields=["sampled"],
        nearest=True,
        on="mouseover",
        empty="none",
    )

    # scale y to the bounds of the given dataframe
    # scale from 0 and it looks like a flat line at the top
    yscale = df[field].agg(['min','max'])
    yscale[0] = max(yscale[0], yscale[0] - 100)
    yscale[1] = yscale[1] + 100
    lines = (
        alt.Chart(data, title="Power consumption over time")
        .mark_line()
        .encode(
            x=alt.X("sampled"),
            y=alt.Y(field, scale=alt.Scale(domain=[yscale[0], yscale[1]])),
        )
    )

    # Draw points on the line, and highlight based on selection
    points = lines.transform_filter(hover).mark_circle(size=65)

    # Draw a rule at the location of the selection
    tooltips = (
        alt.Chart(data)
        .mark_rule()
        .encode(
            x="sampled",
            y=field,
            opacity=alt.condition(hover, alt.value(0.3), alt.value(0)),
            tooltip=[
                alt.Tooltip("yearmonthdatehoursminutes(sampled)", title="Datetime"),
                alt.Tooltip(field, title=units),
            ],
        )
        .add_selection(hover)
    )
    return (lines + points + tooltips).interactive()

# bar chart with daily consumption totals
st.bar_chart(dfday, y="consumed")

st.bar_chart(df, x="sampled", y="consumed")

# timeseries line chart with "meter" usage
dwh_chart = get_chart(df, "value", "DWh")
st.altair_chart(
    dwh_chart.interactive(),
    use_container_width=True
)
