FROM python:slim

# Expose port you want your app on
EXPOSE 8086

# Upgrade pip and install requirements
COPY requirements.txt requirements.txt
RUN pip install -U pip
RUN pip install -r requirements.txt

# Copy app code and set working directory
RUN mkdir /app
COPY app.py /app
WORKDIR /app


CMD ["streamlit", "run", "app.py", "--server.port=8086", "--server.address=0.0.0.0"]